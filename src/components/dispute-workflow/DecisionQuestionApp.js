/* eslint no-unused-vars: 0, eqeqeq:0 */
import React from 'react';
import { connect } from 'react-redux';
import { getDisputeReasonQuestions, setDisputeReasonAnswer } from '../../state/actions/actions';


const QuestionsApp = props => {
    const questionList = props.questions;
    const selectItem = questionList.map((qtem) =>
        <option key={qtem.id} value={qtem.question}>{qtem.question}</option>
    );
    return (
        <div className="DecisionQuestionApp__select">
            <select id="sel" className=" DecisionQuestionApp__select--width form-control" value={props.selectValue} onChange={props.handleSelectAnswer} >
                <option disabled value=''>Select an answer...</option>
                {selectItem}
            </select>
        </div>
    );
}

class DecisionQuestionApp extends React.Component {
    state = {
        confirmCheck: false
    }
    componentDidMount() {
        this.props.getDisputeReasonQuestions();
    }

    handleSelectAnswer = (e) => {
        //this.props.jumpToStep();
        const value = e.target.value;
        this.props.setDisputeReasonAnswer(value);
        // if (value != 'I did not authorize the transaction(s)') {
        //     this.props.jumpToStep(4);
        // }
    }

    confirmed = () => {
        var jsonObj = this.state;
        jsonObj.confirmCheck = true;
        this.setState({
            jsonObj
        });
    }

    render() {
        const confirm = this.state.confirmCheck;
        const currentAnswer = this.props.transactionDisputeReasonAnswers;

        let buttonActive =
            <button name="next" disabled type="button" className="btn btn-default pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(4)} >
                Next</button>;

        if (currentAnswer != 'I did not authorize the transaction(s)' && currentAnswer !== '') {
            buttonActive =
                <button name="next" type="button" className="btn btn-primary pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(4)} >
                    Next</button>;
        }
        if (currentAnswer == 'I did not authorize the transaction(s)') {
            if (confirm === true) {
                buttonActive =
                    <button name="next" type="button" className="btn btn-primary  pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(2)} >
                        Next</button>;
            } else if (confirm === false) {
                buttonActive =
                    <button name="next" disabled type="button" className="btn btn-primary  pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(4)} >
                        Next</button>;
            }
        }

        return (
            <div className="DecisionQuestionApp">
                <br />
                <div className="row">
                    <p className="DecisionQuestionApp__question">Let us ask a few questions</p>
                    <div className="form-group col-sm-6">
                        <label className="DecisionQuestionApp__step" htmlFor="sel">1. Why are you disputing these transactions?</label>
                        <br />
                        <QuestionsApp questions={this.props.transactionDisputeReasonQuestions} handleSelectAnswer={this.handleSelectAnswer} selectValue={this.props.transactionDisputeReasonAnswers} />
                    </div>
                </div>
                <div className="row">
                    {
                        currentAnswer == 'I did not authorize the transaction(s)' ?
                            <div className="form-group col-sm-12 DecisionQuestionApp__spacer">
                                <p className="DecisionQuestionApp__cancel-credit">Due to strict security and privacy regulations, we must cancel your credit card and issue a new one. Please confirm.</p>
                                <input name="yes" type="button" className="btn DecisionQuestionApp__custom-buttons btn-primary neibourb" onClick={() => this.confirmed()} value="Confirm - Reissue Card" />
                                <input name="no" type="button" className="btn DecisionQuestionApp__custom-buttons neibourb" value="No - Cancel" onClick={() => this.props.history.push('/')} />
                            </div>
                            :
                            null
                    }
                </div>
                <div className="row">
                    <button name="prev" type="button" className="btn btn-primary  pull-left stepZillLeftabt" onClick={() => this.props.jumpToStep(0)} ><i className="fa fa-chevron-left"></i></button>
                    {buttonActive}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        transactionDisputeReasonQuestions: state.transactionDisputeReducer.transactionDisputeReasonQuestions,
        transactionDisputeReasonAnswers: state.transactionDisputeReducer.transactionDisputeReasonAnswers
    }
}
const mapDispatchToProps = dispatch => {
    return {
        getDisputeReasonQuestions: () => dispatch(getDisputeReasonQuestions()),
        setDisputeReasonAnswer: (payload) => dispatch(setDisputeReasonAnswer(payload)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(DecisionQuestionApp);