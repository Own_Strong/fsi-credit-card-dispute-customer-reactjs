import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

import { getAdditionalQuestions } from '../../state/actions/actions';
import { setAdditionalQuestionsAnswer } from '../../state/actions/actions';

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
})

const AdditionalQuestions = props => {

    const propsQuestions = props.questions;
    const currentValues = props.currentValues;
    var myQuestions = [];
    var myIDs = [];

    const questions = propsQuestions.map((question) => {
        myQuestions.push(question.question);
        myIDs.push(question.id);
    });

    function print(index) {
        if (index < 0) {
            index = 0;
            props.jumpToStep(2);
        } else if (index > myQuestions.length - 1 && index != 0) {
            // when all the questions are done
            props.jumpToStep(4);
        }
        return (
            <div className="row" key={myIDs[index]} >
                <div className="DecisionQuestionApp__spacer">
                    <div className="col-lg-3">
                        <p> {myQuestions[index]} </p>
                    </div>
                    <div className="col-lg-4">
                        {/* <input type="text" name="answer" onChange={(e) => props.handleAnswer(myIDs[index], e)} value={currentValues[myIDs[index]]} /> */}
                        <label className="radio-inline"><input type="radio" name="optradio" />Yes</label>
                        <label className="radio-inline"><input type="radio" name="optradio" />No</label>
                    </div>
                </div>
                <br />
            </div>
        );

    }

    function goNext() {
        props.increment();
        var temp = props.counter;
        print(temp);
    }
    function goPrev() {
        props.decrement();
        var temp = props.counter;
        print(temp);
    }


    return (
        <div>
            <div className="row">
                <div className="col-lg-12">
                    {questions}
                    {print(props.counter)}
                </div>
            </div>
            <div className="row">
                <button name="prev" type="button" className="btn btn-primary  pull-left stepZillLeftabt" onClick={() => goPrev()} ><i className="fa fa-chevron-left"></i></button>
                <button name="next" type="button" className="btn btn-primary  pull-right stepZillRightabt" onClick={() => goNext()} >Next</button>
            </div>

        </div>
    );
}

class AdditionalQuestionnaire extends Component {
    state = {
        counter: 0
    }
    componentDidMount() {
        this.props.getAdditionalQuestions();
    }

    handleAnswer = (questionId, e) => {
        const target = e.target;
        const value = target.value;

        var currentAnswer = this.props.additionalQuestionAnswers;
        currentAnswer[questionId] = value;
        this.props.setAdditionalQuestionsAnswer(currentAnswer);
    }

    increment = () => {
        var jsonObj = this.state;
        jsonObj.counter = jsonObj.counter + 1;
        this.setState({
            jsonObj
        });
    }

    decrement = () => {
        var jsonObj = this.state;
        jsonObj.counter = jsonObj.counter - 1;
        this.setState({
            jsonObj
        });
    }

    render() {
        const { classes } = this.props;
        let question_content =
            <AdditionalQuestions
                jumpToStep={this.props.jumpToStep}
                increment={this.increment}
                decrement={this.decrement}
                counter={this.state.counter}
                questions={this.props.additionalQuestions}
                handleAnswer={(questionId, e) => this.handleAnswer(questionId, e)}
                currentValues={this.props.additionalQuestionAnswers} />;

        if (this.props.loading) {
            question_content =
                <CircularProgress className={classes.progress} size={50} />;
        }
        if (this.props.error) {
            question_content =
                <div>
                    <p>At the moment, the system cannot connect to the server, please return later. Sorry for the inconvenience.</p>
                    <p>Error:  {this.props.error}</p>
                </div>;
        }

        return (
            <div>
                <br />
                <div className="row">
                    <p className="DecisionQuestionApp__question">Let us ask a few questions</p>
                    <div className="col-lg-12">
                        <label className="DecisionQuestionApp__step"><b> Additional Questions: </b></label>
                    </div>
                    <br />
                    <br />
                </div>
                {question_content}
                <AdditionalQuestions
                    jumpToStep={this.props.jumpToStep}
                    increment={this.increment}
                    decrement={this.decrement}
                    counter={this.state.counter}
                    questions={this.props.additionalQuestions}
                    handleAnswer={(questionId, e) => this.handleAnswer(questionId, e)}
                    currentValues={this.props.additionalQuestionAnswers} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        additionalQuestions: state.transactionDisputeReducer.additionalQuestions,
        additionalQuestionAnswers: state.transactionDisputeReducer.additionalQuestionAnswers,
        loading: state.transactionDisputeReducer.loading,
        error: state.transactionDisputeReducer.error,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        getAdditionalQuestions: () => dispatch(getAdditionalQuestions()),
        setAdditionalQuestionsAnswer: (payload) => dispatch(setAdditionalQuestionsAnswer(payload)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AdditionalQuestionnaire));