/* eslint no-unused-vars: 0, eqeqeq:0 */
import React from 'react'
import { Switch, Route } from 'react-router-dom';

import TransactionHistoryApp from './../containers/TransactionHistoryApp';
import MainForm from './../containers/MainForm'


export default (
    <Switch>
        <Route exact path='/' component={TransactionHistoryApp} />
        <Route exact path='/MainForm' component={MainForm} />
    </Switch>

);



        //<Route exact path='/DisputeCreditTransaction' component={DisputeCreditTransaction} />
       // <Route exact path='/DecisionQuestionApp' component={DecisionQuestionApp} />
       // <Route exact path='/CreditCardQuestionnaire' component={CreditCardQuestionnaire} />
       // <Route exact path='/AdditionalQuestionnaire' component={AdditionalQuestionnaire} />
       // <Route exact path='/ExtraInfo' component={ExtraInfo} />
       // <Route exact path='/DisputeConfirmation' component={DisputeConfirmation} />
       // <Route exact path='/Confirmed' component={Confirmed} />
       // <Route exact path='/Greeting' component={Greeting} />