/* eslint no-unused-vars: 0, eqeqeq:0 */
import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router } from 'react-router-dom';

import routes from './config/routes';

import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk'

import transactionDisputeReducer from './state/reducers/reducers';
import stepper from './steppers/state/reducer';

import 'font-awesome/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import './../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './../node_modules/font-awesome/css/font-awesome.css'
import './../node_modules/animate.css/animate.min.css'

import './styles/style.css'
//comment the top block before deploy to server
////////////////////////////////////////////////////////

import './steps/css/main.css'
import './css/cssoverride.css'

const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

const rootReducer = combineReducers({
    transactionDisputeReducer: transactionDisputeReducer,
    stepper: stepper,
});

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunkMiddleware)
));


ReactDOM.render(
    <Provider store={store} >
        <Router
            basename={'/fsi-credit-card-dispute-customer'}
        >
            {routes}
        </Router>
    </Provider>
    , document.getElementById('fsi-demo-customer'));
registerServiceWorker();
