import { updateObject } from './../../shared/utility';
import * as actionTypes from './types';

const initialState = {
  currentStep: 0,
  totalSteps: 0,
};

const totalSteps = (state, action) => {
  return updateObject(state, {totalSteps: action.payload});
};
const currentStep = (state, action) => {
  return updateObject(state, {currentStep: action.payload});
};
const stepUp = (state, action) => {
  const current_step =  state.currentStep + 1;
  return updateObject(state, {currentStep: current_step});
};
const stepDown = (state, action) => {
  const current_step =  state.currentStep - 1;
  return updateObject(state, {currentStep: current_step});
};

const stepper = (state = initialState, action) => {
  switch (action.type) {
      case actionTypes.SET_TOTAL_STEPS: return totalSteps(state, action);
      case actionTypes.SET_CURRENT_STEP: return currentStep(state, action);
      case actionTypes.STEP_UP: return stepUp(state, action);
      case actionTypes.STEP_DOWN: return stepDown(state, action);
      default: return state;
  }
};

export default stepper;


// const totalSteps = (state = defaultState.totalSteps, action = {}) => {
//   switch (action.type) {
//       case SET_TOTAL_STEPS:
//         return action.payload.totalSteps;
//       default:
//         return state;
//   }
// };

// const currentStep = (state = defaultState.currentStep, action = {}) => {
//   switch (action.type) {
//       case SET_CURRENT_STEP:
//         return action.payload.step;
//       case STEP_UP:
//         return state + 1;
//       case STEP_DOWN:
//         return state - 1;
//       default:
//         return state;
//   }
// };

// export default combineReducers({
//   currentStep,
//   totalSteps,
// });